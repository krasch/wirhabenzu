from pathlib import Path

from jinja2 import Template
from PIL import Image


TEMPLATE_PATH = Path("index.html.template")
RESULT_PATH = Path("index.html")
IMAGE_DIR = Path("images/size800")
THUMBNAIL_DIR = Path("images/size200")

def get_image_info(img: Path):
    thumbnail = THUMBNAIL_DIR / img.name
    thumbnail_width, thumbnail_height = Image.open(thumbnail).size
    return {"image": "{}/{}".format(IMAGE_DIR.relative_to("."), img.name),
            "thumbnail": "{}/{}".format(THUMBNAIL_DIR.relative_to("."), img.name),
            "thumbnail_width": thumbnail_width,
            "thumbnail_height": thumbnail_height}


assert IMAGE_DIR.exists()
assert THUMBNAIL_DIR.exists()
images = [get_image_info(i) for i in sorted(IMAGE_DIR.glob("*.jpg"))]

template = Template(TEMPLATE_PATH.read_text())
result = template.render(images=images)
RESULT_PATH.write_text(result)
