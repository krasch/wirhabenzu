rm -rf size800
cp -r cutout size800
cd size800
mogrify -auto-orient *.jpg
mogrify -strip *.jpg
find ./*.jpg -exec convert {} -resize "800x800" {} \; 
cd ..
rm -rf size200
cp -r size800 size200
cd size200
find ./*.jpg -exec convert {} -resize "200x200" {} \; 
mogrify -format jpg -quality 80 *.jpg
